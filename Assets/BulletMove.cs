﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

    public float speed;

    private Rigidbody2D rigid;

    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

	// Use this for initialization
	void OnEnable () {
        rigid.velocity = Vector2.zero;
        Vector2 amt = transform.right * speed * Time.deltaTime;
        rigid.AddForce(amt, ForceMode2D.Impulse);
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
