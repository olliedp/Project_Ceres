﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLookAtTarget : MonoBehaviour {

    public Transform target;
    public float speed;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        float step = speed * Time.fixedDeltaTime;
        Vector3 targetRot = target.rotation.eulerAngles;
        targetRot.z -= 90;
        Quaternion to = Quaternion.Euler(targetRot);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, to, speed);
    }
}
