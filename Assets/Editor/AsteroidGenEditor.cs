﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(AsteroidGenerator))]
public class AsteroidGenEditor : Editor
{
    public override void OnInspectorGUI()
    {
        AsteroidGenerator astGen = (AsteroidGenerator)target;

        DrawDefaultInspector();

        if (GUILayout.Button("Generate"))
        {
            Mesh m = astGen.Generate();
            foreach (Vector3 vertex in m.vertices)
            {
                Debug.Log(vertex);
            }
        }
    }
}
