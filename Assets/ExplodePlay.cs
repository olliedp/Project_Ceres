﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodePlay : MonoBehaviour {

    private Animator explodeAnimation;

    void Awake()
    {
        explodeAnimation = GetComponent<Animator>();
    }

	void Start()
    {
        explodeAnimation.SetTrigger("Explode");
    }
}
