﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour {

    public GameObject area;
    public GameObject destination;
    public GameObject destinationTele;
    public GameObject miniCam;
}
