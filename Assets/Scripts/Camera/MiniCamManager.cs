﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniCamManager : MonoBehaviour {

    [SerializeField]
    private GameObject activeMiniCam;

    public void SetActiveMiniCam(GameObject cam)
    {
        activeMiniCam.SetActive(false);
        cam.SetActive(true);
    }
}
