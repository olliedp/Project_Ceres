﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamIgnoreLight : MonoBehaviour {

    public GameObject lightSource;

	void OnPreCull()
    {
        lightSource.SetActive(false);
    }

    void OnPostRender()
    {
        lightSource.SetActive(true);
    }
}
