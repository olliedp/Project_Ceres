﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public Sprite idleSprite, jumpSprite;

    private bool runningLeft, runningRight;
    private bool jumping = false;
    private bool grounded = false;
    private bool moving;
    public bool Jumping
    {
        get { return jumping; }
    }

    private Animator animator;
    protected Rigidbody2D rigid;
    protected SpriteRenderer spriteRender;

	// Use this for initialization
	protected void Start ()
    {
        animator = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody2D>();
        spriteRender = GetComponent<SpriteRenderer>();
	}
	
    protected void Update()
    {
        moving = animator.GetBool("movingright");

        if (jumping)
            grounded = false;

        // Jump
        if (!jumping && Input.GetKeyDown(KeyCode.Space))
        {
            jumping = true;
            rigid.velocity += (Vector2)transform.up * 2.5f;
            runningLeft = false;
            runningRight = false;
            moving = false;
        }

        // Check input for moving left/right
        if (Input.GetKeyDown(KeyCode.D))
        {
            //spriteRender.flipX = false;
            runningRight = true;
            runningLeft = false;
            moving = true;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            //spriteRender.flipX = true;
            runningLeft = true;
            runningRight = false;
            moving = true;
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            runningLeft = false;
            moving = false;
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            runningRight = false;
            moving = false;
        }

        animator.SetBool("movingright", moving);

        Debug.Log(grounded);
    }

	// Update is called once per frame
	protected void FixedUpdate ()
    {

        if (!jumping)
        {
            // Limit player's speed 
            rigid.velocity = Vector2.ClampMagnitude(rigid.velocity, 1f);

            if (grounded)
            {
                // Actually move left/right and check when move buttons are released
                if (runningRight)
                    rigid.velocity += (Vector2)transform.right * 5 * Time.deltaTime;


                if (runningLeft)
                    rigid.velocity += -(Vector2)transform.right * 5 * Time.deltaTime;
            }
            
        }

        else
            spriteRender.sprite = jumpSprite;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        //Landed on some ground; update movement

        if (coll.gameObject.tag == "Planet")
        {
            jumping = false;
            grounded = true;

            spriteRender.sprite = idleSprite;

            if (Input.GetKey(KeyCode.D))
            {
                runningRight = true;
                animator.SetBool("movingright", true);
            }

            if (Input.GetKey(KeyCode.A))
            {
                runningLeft = true;
                animator.SetBool("movingright", true);
            }
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (jumping && coll.gameObject.tag == "Planet")
            grounded = false;
    }
}
