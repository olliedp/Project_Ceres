﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeleport : MonoBehaviour {

    private bool teleNear = false;

    private Teleporter nearbyTele;

    [SerializeField]
    private MiniCamManager mcm;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && teleNear)
        {
            nearbyTele.destination.SetActive(true);
            transform.position = nearbyTele.destinationTele.transform.position;
            nearbyTele.area.SetActive(false);
            mcm.SetActiveMiniCam(nearbyTele.miniCam);
        }
    }

	void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Teleporter")
        {
            teleNear = true;
            nearbyTele = coll.GetComponent<Teleporter>();
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Teleporter")
            teleNear = false;
    }
}
