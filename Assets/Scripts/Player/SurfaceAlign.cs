﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceAlign : MonoBehaviour {

    private PlanetTarget target;

    private PlayerMove playerMove;

    private Ray lRay, rRay;

    void Awake()
    {
        target = GetComponent<PlanetTarget>();
        playerMove = GetComponent<PlayerMove>();
    }

	// Use this for initialization
	void Start ()
    {
        lRay = new Ray(transform.position - transform.right * 5, -transform.up * 2.5f);
        rRay = new Ray(transform.position + transform.right * 5, -transform.up * 2.5f);
    }


    // Update is called once per frame
    void Update ()
    {
        // Set left and right raycasts
        lRay.origin = transform.position - transform.right * 0.04f;
        lRay.direction = -transform.up * 10;

        rRay.origin = transform.position + transform.right * 0.04f;
        rRay.direction = -transform.up * 10;

        RaycastHit2D l = Physics2D.Raycast(lRay.origin, lRay.direction, 1);
        RaycastHit2D r = Physics2D.Raycast(rRay.origin, rRay.direction, 1);

        Vector2 avgNormal = (l.normal + r.normal) / 2;
        Vector2 avgPoint = (l.point + r.point) / 2;
        
        // If raycast hit planet/asteroid, align player with the surface
        if (l.collider != null && l.collider.gameObject.tag == "Planet")
        {
            Quaternion targetRot = Quaternion.FromToRotation(Vector2.up, avgNormal);
            Quaternion finalRot = Quaternion.RotateTowards(transform.rotation, targetRot, 5f);

            transform.rotation = Quaternion.Euler(0, 0, finalRot.eulerAngles.z);
        }

        // Angle towards the planet if too far away
        else if (l.collider == null)
        {
            Transform t = target.value.transform;

            float angle = (180 / Mathf.PI) * Mathf.Atan2(t.position.y - transform.position.y, t.position.x - transform.position.x);
            transform.rotation = Quaternion.Euler(0, 0, angle + 90);
        }
    }
}
