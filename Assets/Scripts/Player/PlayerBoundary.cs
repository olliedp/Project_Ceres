﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBoundary : MonoBehaviour {

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Boundary")
        {
            transform.position = Vector3.zero;
        }
    }
}
