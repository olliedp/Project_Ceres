﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    [SerializeField]
    private float value = 50;

    private float currentHealth;

    private bool dead = false;

    private IKillable killScript;
    private IDamagable damageScript;

    void Awake()
    {
        killScript = GetComponent<IKillable>();
        damageScript = GetComponent<IDamagable>();
        currentHealth = value;
    }

    public void Damage(float amount)
    {
        if (!dead)
        {
            value -= amount;
            if (!dead && value <= 0)
            {
                dead = true;
                killScript.Kill();
            }
            else
                damageScript.TakeDamage(amount);
        }
        
    }
}
