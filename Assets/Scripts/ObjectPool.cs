﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

    public GameObject pooledObject;
    public uint size;

    private List<GameObject> pool;

	// Use this for initialization
	void Awake () {
        pool = new List<GameObject>();
	}

    void Start()
    {
        for (int i = 0; i < size; i++)
        {
            GameObject obj = Instantiate(pooledObject);
            obj.SetActive(false);
            pool.Add(obj);
        }
    }
	
    public GameObject Pick()
    {
        for (int i = 0; i < size; i++)
        {
            if (!pool[i].activeSelf)
            {
                return pool[i];
            }
        }

        return null;
    }
	// Update is called once per frame
	void Update () {
		
	}
}
