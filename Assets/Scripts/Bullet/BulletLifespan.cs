﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLifespan : MonoBehaviour
{
    public float time = 4;
    private float elapsed = 0;

    void OnEnable()
    {
        elapsed = 0;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (elapsed >= time)
            gameObject.SetActive(false);
        else
            elapsed += Time.deltaTime;	
	}
}
