﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFire : MonoBehaviour {

    public GameObject gun;
    private SpriteRenderer gunSprite;

    private Rigidbody2D rigid;

    public Vector3 offset;

    private Vector2 force;

    void Awake()
    {
        gun = GameObject.FindGameObjectWithTag("Gun");
        gunSprite = gun.GetComponent<SpriteRenderer>();
    }

	void OnEnable()
    {
        transform.position = gun.transform.position + (gun.transform.rotation * new Vector3(0.07f, 0.005f, -0.03f));
        Vector3 angles = gun.transform.localEulerAngles;
        transform.rotation = Quaternion.Euler(angles);

        rigid = GetComponent<Rigidbody2D>();

        
    }

    void Update()
    {
        rigid.AddForce(transform.right/50, ForceMode2D.Impulse);
    }

    void OnBecameInvisible()
    {
        gameObject.SetActive(false);
    }
}
