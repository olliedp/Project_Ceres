﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunProperties : MonoBehaviour {

    public bool reloading;
    public bool firing;
    public uint clipSize;
    public uint ammo;
    public float reloadSpeed;
    public float fireSpeed;
    public float damage;

    public ObjectPool bulletPool;
}
