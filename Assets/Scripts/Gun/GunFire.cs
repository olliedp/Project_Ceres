﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFire : MonoBehaviour {

    public float forceMultiplier;

    private Vector3 force;              // Force applied to player upon firing

    private Rigidbody2D playerBody;
    private PlayerMove playerMove;

    public Sprite firingSprite;
    private Sprite defaultSprite;

    private GunProperties gun;
    private SpriteRenderer gunSprite;

    private SpriteRenderer bulletSprite;

    private Ray fireRay;

    private uint bulletsInClip;

    private bool firing = false;

	// Use this for initialization
	void Awake()
    {
        playerBody = GetComponentInParent<Rigidbody2D>();
        playerMove = GetComponentInParent<PlayerMove>();

        gun = GetComponent<GunProperties>();
        gunSprite = GetComponent<SpriteRenderer>();
        defaultSprite = gunSprite.sprite;

        bulletSprite = GetComponent<SpriteRenderer>();

        bulletsInClip = gun.clipSize;
        fireRay = new Ray(transform.position, transform.right);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && !gun.reloading && !gun.firing)
        {
            
            fireRay.origin = transform.position;

            bulletSprite.flipX = gunSprite.flipX;

            if (gunSprite.flipX)
                fireRay.direction = -transform.right;
            else
                fireRay.direction = transform.right;

            
            if (bulletsInClip > 0)
            {
                bulletsInClip--;
                //gun.bulletPool.Pick();
                if (!playerMove.Jumping)
                    force = Vector2.zero;
                else if (gunSprite.flipX)
                    force = transform.right * forceMultiplier * Time.deltaTime;
                else
                    force = -transform.right * forceMultiplier * Time.deltaTime;

                playerBody.AddForce(force, ForceMode2D.Impulse);
                StartCoroutine(ChangeSprite());
                StartCoroutine(Fire());
                
            }
            else if (bulletsInClip == 0)
            {
                StartCoroutine(Reload());
            }
        }

        if (Input.GetKeyDown(KeyCode.R) && !gun.reloading)
        {
            StartCoroutine(Reload());
        }
    }

    private IEnumerator Reload()
    {
        gun.reloading = true;
        yield return new WaitForSeconds(gun.reloadSpeed);
        bulletsInClip = gun.clipSize;
        gun.reloading = false;
    }

    void OnGUI()
    {
        //Draw ammo count
        GUI.color = Color.white;

        GUI.Label(new Rect(10, 10, 200, 50), "Clip: " + bulletsInClip + "/" + gun.clipSize);
        if (gun.reloading)
        {
            GUI.Label(new Rect(10, 30, 200, 50), "Reloading...");
        }
    }

    void OnDrawGizmos()
    {
        // Draw velocity vector
        if (playerBody != null)
        {
            Gizmos.DrawRay(transform.position, playerBody.velocity);
        }
    }

    private IEnumerator ChangeSprite()
    {
        gunSprite.sprite = firingSprite;
        yield return new WaitForSeconds(0.125f/2);
        gunSprite.sprite = defaultSprite;
    }

    private IEnumerator Fire()
    {
        gun.firing = true;
        RaycastHit2D hit = Physics2D.Raycast(fireRay.origin, fireRay.direction, 25);
        if (hit && hit.collider.tag == "Enemy")
        {
            Health enemyHealth = hit.collider.GetComponent<Health>();
            enemyHealth.Damage(gun.damage);
        }
        yield return new WaitForSeconds(gun.fireSpeed);
        gun.firing = false;
    }
}
