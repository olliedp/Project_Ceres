﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunForce : MonoBehaviour
{
    private GunProperties gun;

    public float forceMultiplier;
    
    private bool reloading;

    private Vector3 force;              // Force applied to player upon firing

    private Rigidbody2D playerBody;     
    private PlayerMove playerMove;      
    private SpriteRenderer spriteRender;

	// Use this for initialization
	void Start ()
    {
        gun = GetComponent<GunProperties>();

        playerBody = GetComponentInParent<Rigidbody2D>();
        playerMove = GetComponentInParent<PlayerMove>();

        spriteRender = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Add force when firing gun and not reloading
        if (Input.GetMouseButtonDown(0) && !gun.reloading && !gun.firing && playerMove.Jumping)
        {
            // Apply force to the player if they're jumping

        }
	}

    
}
