﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour {

    private float angle = 0.0f;             // Angle used for pointing to the cursor

    public SpriteRenderer playerSprite;     // Player (parent) components
    public Transform parent;

    private Vector3 position;
    private SpriteRenderer spriteRender;

	// Use this for initialization
	void Start () {
        spriteRender = GetComponent<SpriteRenderer>();
        transform.localPosition = new Vector3(-0.015f, 0.02f, -0.03f);
	}
	
	// Update is called once per frame
	void LateUpdate () {

        // Calculate the angle to point towards the mouse pointer
        float camDis = Camera.main.transform.position.y - transform.position.y;
        Vector3 mouse = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, camDis));
        angle = (180 / Mathf.PI) * Mathf.Atan2(mouse.y - transform.position.y, mouse.x - transform.position.x);

        transform.rotation = Quaternion.Euler(0, 0, angle);


        // Check whether the sprites are flipped and update the gun's rotation accordingly
        float z = transform.localRotation.eulerAngles.z;
        if (!spriteRender.flipX)
        {
            // If gun is pointed left while facing right, face left.
            if (z > 90 && z < 270)
            {
                spriteRender.flipX = true;
                playerSprite.flipX = true;

                if (transform.localPosition.x != 0.015f)
                    transform.localPosition = new Vector3(0.015f, 0.02f, -0.03f);

                transform.rotation = Quaternion.Euler(0, 0, angle - 180);
            }
            else
                transform.rotation = Quaternion.Euler(0, 0, angle);
        }


        // Gun is flipped, so flip the angle when updating rotation
        else
        {
            if (z < 90 || z > 270)
            {
                spriteRender.flipX = false;
                playerSprite.flipX = false;

                if (transform.localPosition.x != -0.015f)
                    transform.localPosition = new Vector3(-0.015f, 0.02f, -0.03f);

                transform.rotation = Quaternion.Euler(0, 0, angle);
            }
            else
                transform.rotation = Quaternion.Euler(0, 0, angle - 180);
        }
    }
}
