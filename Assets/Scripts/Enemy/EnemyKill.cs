﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKill : MonoBehaviour, IKillable
{
    public GameObject explosion;

    public Sprite hitSprite;

    private SpriteRenderer spriteRender;

	// Use this for initialization
	void Start () {
        spriteRender = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Kill()
    {
        spriteRender.sprite = hitSprite;
        Vector3 pos = transform.position;
        pos.z = -1;
        Instantiate(explosion, pos, transform.rotation);
        StartCoroutine(Explode(4, 0.125f));
        StartCoroutine(Flicker(0.5f));
    }

    private IEnumerator Flicker(float time)
    {
        float elapsed = 0;
        while (elapsed < time)
        {
            spriteRender.sprite = hitSprite;
            spriteRender.enabled = !spriteRender.enabled;
            yield return new WaitForSeconds(time/10);
            elapsed += time / 10;
        }
        Destroy(gameObject);
    }

    private IEnumerator Explode(int explodeNum, float explodeDelay)
    {
        for(int i = 0; i < explodeNum; i++)
        {
            Vector3 offset = transform.position + (Vector3)Random.insideUnitCircle/2;
            offset.z = -1;
            Instantiate(explosion, offset, transform.rotation);
            yield return new WaitForSeconds(explodeDelay);
        }
        Destroy(gameObject);
    }
}
