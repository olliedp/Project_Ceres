﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    public float maxVelocity = 5;
    public float attractMultiplier = 2;
    public float repelMultiplier = 4;

    public Rigidbody2D target;
    private PlanetTarget planet;
    private Transform planTransf;
    private PlayerMove playerMove;

    private Rigidbody2D rigid;

    // Use this for initialization
    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();

        planet = GetComponent<PlanetTarget>();
        planTransf = planet.value.transform;

        playerMove = target.GetComponent<PlayerMove>();
    }

    void OnEnable()
    {
        rigid = GetComponent<Rigidbody2D>();

        planet = GetComponent<PlanetTarget>();
        planTransf = planet.value.transform;

        playerMove = target.GetComponent<PlayerMove>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(target.position);
        // Set angle towards closest planet (may need its own script)
        planTransf = planet.value.transform;
        float angle = (180 / Mathf.PI) * Mathf.Atan2(planTransf.position.y - transform.position.y, planTransf.position.x - transform.position.x);
        transform.rotation = Quaternion.Euler(0, 0, angle + 90);
    }

    void FixedUpdate()
    {
        float distance = (target.position - (Vector2)transform.position).magnitude;

        
            Vector2 force = -((Vector2)transform.position - target.position).normalized * attractMultiplier * Time.fixedDeltaTime;
            rigid.AddForce(force);
        

        rigid.velocity = Vector2.ClampMagnitude(rigid.velocity, maxVelocity);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.tag == "Planet")
        {
            rigid.AddForce(transform.up * repelMultiplier * Time.fixedDeltaTime);
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
    }
}
