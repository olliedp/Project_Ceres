﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour, IDamagable {

    public Sprite hitSprite;

    private SpriteRenderer spriteRender;
    private Sprite enemySprite;

    private Animator animator;

    private Health health;

    void Awake()
    {
        spriteRender = GetComponent<SpriteRenderer>();
        enemySprite = spriteRender.sprite;

        animator = GetComponent<Animator>();

        health = GetComponent<Health>();
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TakeDamage(float amt)
    {
        StartCoroutine(ChangeSprite(0.125f));
    }

    private IEnumerator ChangeSprite(float time)
    {
        if (animator != null)
            animator.enabled = false;
        spriteRender.sprite = hitSprite;
       yield return null;
        spriteRender.sprite = enemySprite;
        if (animator != null)
            animator.enabled = true;
    }
}
