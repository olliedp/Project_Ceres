﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOrbit : MonoBehaviour
{

    public Rigidbody2D target;

    public float speed;

    private Vector3 zAxis;

    void Start()
    {
        zAxis = new Vector3(0, 0, 1);
    }

    void FixedUpdate()
    {
        transform.RotateAround(target.worldCenterOfMass, zAxis, speed * Time.fixedDeltaTime);
    }
}
