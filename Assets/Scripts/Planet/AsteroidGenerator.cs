﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AsteroidGenerator : MonoBehaviour {

    public int n;
    public float radius;

    [Range(0, 10)]
    public float xMultiplier, yMultiplier;

    public string seed;
    public bool useRandomSeed;

    public List<Vector2> points;

    public Material minimapMaterial;

    private PolygonCollider2D polyCollider;
    private Rigidbody2D rigid;

    private System.Random randGen;

	// Use this for initialization
	void Start () {

	}
	
	public Mesh Generate()
    {
        points = new List<Vector2>();
        polyCollider = GetComponent<PolygonCollider2D>();
        rigid = GetComponent<Rigidbody2D>();

        if (useRandomSeed)
            seed = Time.time.ToString();

        randGen = new System.Random(seed.GetHashCode());

        float rand = randGen.Next(0, 10);

        // Generate collider points 
        for (float i = 0; i < 2 * Mathf.PI; i += (2 * Mathf.PI / n))
        {
            float x = Mathf.Cos(i);
            float y = Mathf.Sin(i);

            float offset = Mathf.PerlinNoise(rand + x, rand + y);

            var point = new Vector2(x + offset * xMultiplier, y + offset * yMultiplier) * radius;
            points.Add(point);
        }

        polyCollider.points = points.ToArray();

        // Generate uv for textures
        Triangulator tr = new Triangulator(points.ToArray());
        int[] indices = tr.Triangulate();

        Vector3[] vertices = new Vector3[points.Count];
        for (int i = 0; i < vertices.Length; i++)
            vertices[i] = new Vector3(points[i].x, points[i].y, 0);

        Vector2[] uvs = new Vector2[vertices.Length];

        List<Vector2> uv = new List<Vector2>();
        for (int i = 0; i < uvs.Length; i++)
        {
            uv.Add(new Vector2(0.5f + vertices[i].x / (2 * radius),
                                0.5f + vertices[i].y / (2 * radius)));
        }
       
        // Create and add mesh
        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = indices;
        mesh.SetUVs(0, uv);
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        if (GetComponent<MeshRenderer>() == null)
            gameObject.AddComponent(typeof(MeshRenderer));


        MeshFilter filter = gameObject.GetComponent<MeshFilter>();

        if (filter == null)
            filter = gameObject.AddComponent<MeshFilter>();

        filter.mesh = mesh;

        CircleCollider2D circle = GetComponent<CircleCollider2D>();

        if (GetComponent<CircleCollider2D>() == null)
            circle = gameObject.AddComponent<CircleCollider2D>();

        circle.isTrigger = true;
        circle.radius = radius + 2.5f;
        circle.offset = rigid.centerOfMass;

        if (transform.childCount > 0)
        {
            foreach (Transform child in transform)
                DestroyImmediate(child.gameObject);
        }

        GameObject obj = new GameObject();
        obj.tag = "Minimap";
        obj.layer = LayerMask.NameToLayer("Minimap");
        obj.transform.parent = transform;
        obj.transform.Translate(transform.position);

        MeshFilter childFilter = obj.AddComponent(typeof(MeshFilter)) as MeshFilter;
        childFilter.mesh = mesh;

        MeshRenderer childMesh = obj.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
        childMesh.material = minimapMaterial;

        return mesh;
    }
}
