﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToPlanet : MonoBehaviour
{
    private PlanetTarget target;
    private Rigidbody2D gravBody;

    private Rigidbody2D body;

    private GravPull gravity;

    private bool rotating = false;

    void Awake()
    {
        target = GetComponent<PlanetTarget>();
        gravBody = target.value.GetComponent<Rigidbody2D>();
        body = GetComponent<Rigidbody2D>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        // If the trigger is a new planet,
        // Update closest object and rotate towards it
        if (coll.tag == "Planet" && coll.gameObject != gravBody.gameObject)
        {
            target.value = coll.gameObject;
            gravBody = target.value.GetComponent<Rigidbody2D>();

            StartCoroutine(ChangeRotation(0.5f));
        }
    }

    // Change rotation to match closest object over time seconds
    private IEnumerator ChangeRotation(float time)
    {
        rotating = true;

        float elapsed = 0.0f;
        float rotation = transform.rotation.eulerAngles.z;
        float toRotation = GetAngle();

        while (elapsed < time)
        {
            float z = Mathf.SmoothStep(rotation, toRotation, elapsed / time);
            transform.rotation = Quaternion.Euler(0, 0, z);
            elapsed += Time.deltaTime;
            yield return null;
        }

        rotating = false;
    }

    // Angle between player and closest object
    private float GetAngle()
    {
        return (180 / Mathf.PI)
            * Mathf.Atan2(gravBody.position.y - body.position.y,
                            gravBody.position.x - body.position.x) + 90;
    }
}
