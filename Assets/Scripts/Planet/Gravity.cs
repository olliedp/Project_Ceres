﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour {

    public float mass = 50.0f;

    void Start()
    {
        GetComponent<Rigidbody2D>().mass = mass;
    }
}
