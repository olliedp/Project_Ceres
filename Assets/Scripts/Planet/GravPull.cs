﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravPull : MonoBehaviour
{
    private const float G = 6.67408e-1f;    // Gravitational constant

    private bool rotating = false;          // Rotation coroutine flag

    private PlanetTarget target;            // Closest gravitational object
    private AsteroidProperties targetProps; 

    private Rigidbody2D body;               // Rigidbody component    
    private GameObject[] gravObjects;       // List of objects that have gravity
    private Rigidbody2D gravBody;           // Closest obj's rigidbody

	// Use this for initialization
	void Start ()
    {
        target = GetComponent<PlanetTarget>();
        targetProps = target.value.GetComponent<AsteroidProperties>();

        body = GetComponent<Rigidbody2D>();

        gravObjects = GameObject.FindGameObjectsWithTag("Planet");

        gravBody = target.value.GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void FixedUpdate ()
    {
        // Apply universal gravitational law to the player
        float m1 = body.mass;
        float m2 = gravBody.mass;

        Vector2 dist = body.position - (gravBody.position + gravBody.centerOfMass);
        Debug.Log(gravBody.centerOfMass);

        float rSqr = dist.sqrMagnitude;

        float gravity = G * (m1 * m2 / rSqr);

        Vector2 force = -dist.normalized * gravity;

        body.AddForce(force * Time.fixedDeltaTime);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        // If the trigger is a new planet,
        // Update closest object and rotate towards it
        if (coll.tag == "Planet" && coll.gameObject != gravBody.gameObject)
        {
            target.value = coll.gameObject;
            targetProps = target.value.GetComponent<AsteroidProperties>();
            gravBody = target.value.GetComponent<Rigidbody2D>();
        }
    }
}
