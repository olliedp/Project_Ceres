﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

    public Transform player;
    public ObjectPool bullets;

    public float fireRate;

    private bool playerNear = false;
    private float elapsed;

	// Use this for initialization
	void Start ()
    {
        elapsed = fireRate;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (playerNear)
        {
            float angle = (180 / Mathf.PI) * Mathf.Atan2(player.position.y - transform.position.y, player.position.x - transform.position.x);
            Quaternion rot = Quaternion.Euler(0, 0, angle);
            if (elapsed >= fireRate)
            {


                GameObject bullet = bullets.Pick();
                bullet.transform.position = transform.position;
                bullet.transform.rotation = rot;
                bullet.SetActive(true);
                elapsed = 0.0f;
            }
            else
            {
                elapsed += Time.deltaTime;
            }
        }

        else
        {
            elapsed = fireRate;
        }
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player")
            playerNear = true;
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Player")
            playerNear = false;
    }
}
